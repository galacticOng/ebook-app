﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace EBookApp.Views
{
    public partial class VolumeDetailPage : ContentPage
    {
        public VolumeDetailPage()
        {
            InitializeComponent();
        }

        void BackTapGestureRecognizer_Tapped(System.Object sender, System.EventArgs e)
        {
            Navigation.PopAsync();
        }
    }
}
