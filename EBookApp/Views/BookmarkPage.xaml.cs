﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using EBookApp.Models;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace EBookApp.Views
{
    public partial class BookmarkPage : ContentPage
    {
        public ObservableCollection<VolumeInfo> BookmarkList { get; set; } = new ObservableCollection<VolumeInfo>();

        public BookmarkPage()
        {
            InitializeComponent();

            GetBookmark();
            BindingContext = this;
        }

        private async void GetBookmark()
        {
            try
            {
                var httpClient = new HttpClient();
                var response = await httpClient.GetStringAsync($"https://www.googleapis.com/books/v1/volumes?q=Xamarin");
                var result = JsonConvert.DeserializeObject<Root>(response);

                BookmarkList.Clear();
                foreach (var item in result.items)
                {
                    BookmarkList.Add(item.volumeInfo);
                }
            }
            catch (Exception ex)
            {

            }
        }

        public Command<VolumeInfo> RemoveCommand => new Command<VolumeInfo>((item) =>
        {
            BookmarkList.Remove(item);
        });
    }
}
