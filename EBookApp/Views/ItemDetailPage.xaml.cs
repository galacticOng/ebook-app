﻿using System.ComponentModel;
using Xamarin.Forms;
using EBookApp.ViewModels;

namespace EBookApp.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}
