﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using EBookApp.Models;
using Xamarin.Forms;

namespace EBookApp.Views
{
    public partial class HomePage : ContentPage
    {
        public ObservableCollection<Book> BookList { get; set; } = new ObservableCollection<Book>();

        public Book BookDetail { get; set; }

        public HomePage()
        {
            InitializeComponent();

            BookList = new ObservableCollection<Book>
            {
                new Book { Name = "The King of Drugs", Thumbnail = "https://d1csarkz8obe9u.cloudfront.net/posterpreviews/action-thriller-book-cover-design-template-3675ae3e3ac7ee095fc793ab61b812cc_screen.jpg?ts=1588152105", Description = "Psychiatric rehabilitation refers to community treatment of people with mental disorders. Community treatment has recently become far more widespread due to deinstitutionalization at government facilities. This book is an update of the first edition's discussion of types of mental disorders, including etiology, symptoms, course, and outcome, types of community treatment programs, case management strategies, and vocational and educational rehabilitation. Providing a comprehensive overview of this rapidly." },
                new Book { Name = "Memory", Thumbnail = "https://d1csarkz8obe9u.cloudfront.net/posterpreviews/contemporary-fiction-night-time-book-cover-design-template-1be47835c3058eb42211574e0c4ed8bf_screen.jpg?ts=1594616847", Description = "In Lara Avery's heartfelt, funny and bittersweet new novel, a gifted teen's future is derailed when she's diagnosed with a debilitating genetic condition. High school valedictorian Sammie McCoy can't wait to escape small-town Vermont and start college at NYU" },
                new Book { Name = "The Gravity of Us", Thumbnail = "https://assets.teenvogue.com/photos/5cd4384fac4d9e712fe2ebb0/2:3/w_1852,h_2778,c_limit/The%20Gravity%20of%20Us_.jpg", Description = "As a successful social media journalist with half a million followers, seventeen-year-old Cal is used to sharing his life online. But when his pilot father is selected for a highly publicized NASA mission to Mars, Cal and his family relocate from Brooklyn to Houston and are thrust into a media circus." },
                new Book { Name = "Book Cover Design Form", Thumbnail = "https://images-na.ssl-images-amazon.com/images/I/61ZKNw0xixL.jpg", Description = "What if I told you, you can create a book cover using the same tricks and tactics that professional designers use? Would you want to learn how?" },
                new Book { Name = "Journey to the Stars", Thumbnail = "https://marketplace.canva.com/EAD7YHrjZYY/1/0/1003w/canva-blue-illustrated-stars-children%27s-book-cover-haFtaSNXXF4.jpg", Description = "Journey to the Stars began as a planetarium show produced by the American Museum of Natural History (AMNH) and supported by NASA's Heliophysics Division. It surveys the mind-boggling variety of stars that dot the cosmos—exploding stars, giant stars, dwarf stars, neutron stars, even our own star!"}
            };

            BindingContext = this;
        }

        public Command GoToBookDetailCommand => new Command<Book>((item) =>
        {
            BookDetail = item;
            Navigation.PushAsync(new BookDetailPage
            {
                BindingContext = this
            });
        });

        void TapGestureRecognizer_Tapped(System.Object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new SearchPage());
        }
    }
}
