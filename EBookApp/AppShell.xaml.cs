﻿using System;
using System.Collections.Generic;
using EBookApp.ViewModels;
using EBookApp.Views;
using Xamarin.Forms;

namespace EBookApp
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();
            Routing.RegisterRoute(nameof(ItemDetailPage), typeof(ItemDetailPage));
            Routing.RegisterRoute(nameof(NewItemPage), typeof(NewItemPage));
        }

    }
}
