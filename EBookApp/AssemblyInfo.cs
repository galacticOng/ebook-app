﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

[assembly: ExportFont("FontAwesome-Regular-400.otf", Alias = "FontAwesomeRegular")]
[assembly: ExportFont("FontAwesome-Solid-900.otf", Alias = "FontAwesomeSolid")]
[assembly: ExportFont("FontAwesome-Brands-400.otf", Alias = "FontAwesomeBrands")]